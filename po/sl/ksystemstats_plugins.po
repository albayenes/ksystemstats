# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the ksysguard package.
#
# Matjaž Jeran <matjaz.jeran@amis.net>, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: ksysguard\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-24 09:33+0000\n"
"PO-Revision-Date: 2022-09-08 06:52+0200\n"
"Last-Translator: Matjaž Jeran <matjaz.jeran@amis.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.04.1\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"

#: cpu/cpu.cpp:28 disks/disks.cpp:67 disks/disks.cpp:68 gpu/GpuDevice.cpp:20
#: power/power.cpp:39
#, kde-format
msgctxt "@title"
msgid "Name"
msgstr "Ime"

#: cpu/cpu.cpp:38
#, kde-format
msgctxt "@title"
msgid "Total Usage"
msgstr "Skupaj porabljeno"

#: cpu/cpu.cpp:39
#, kde-format
msgctxt "@title, Short for 'Total Usage'"
msgid "Usage"
msgstr "Porabljeno"

#: cpu/cpu.cpp:45
#, kde-format
msgctxt "@title"
msgid "System Usage"
msgstr "Porabljeno za sistem"

#: cpu/cpu.cpp:46
#, kde-format
msgctxt "@title, Short for 'System Usage'"
msgid "System"
msgstr "Sistem"

#: cpu/cpu.cpp:52
#, kde-format
msgctxt "@title"
msgid "User Usage"
msgstr "Poraba uporabnika"

#: cpu/cpu.cpp:53
#, kde-format
msgctxt "@title, Short for 'User Usage'"
msgid "User"
msgstr "Uporabnik"

#: cpu/cpu.cpp:59
#, kde-format
msgctxt "@title"
msgid "Wait Usage"
msgstr "Poraba za čakanje"

#: cpu/cpu.cpp:60
#, kde-format
msgctxt "@title, Short for 'Wait Load'"
msgid "Wait"
msgstr "Čakanje"

#: cpu/cpu.cpp:85
#, kde-format
msgctxt "@title"
msgid "Current Frequency"
msgstr "Trenutna frekvenca"

#: cpu/cpu.cpp:86
#, kde-format
msgctxt "@title, Short for 'Current Frequency'"
msgid "Frequency"
msgstr "Frekvenca"

#: cpu/cpu.cpp:87
#, kde-format
msgctxt "@info"
msgid "Current frequency of the CPU"
msgstr "Trenutna frekvenca CPE"

#: cpu/cpu.cpp:92
#, kde-format
msgctxt "@title"
msgid "Current Temperature"
msgstr "Trenutna temperatura"

#: cpu/cpu.cpp:93
#, kde-format
msgctxt "@title, Short for Current Temperatur"
msgid "Temperature"
msgstr "Temperatura"

#: cpu/cpu.cpp:100
#, kde-format
msgctxt "@title"
msgid "All"
msgstr "Vse"

#: cpu/cpu.cpp:119
#, kde-format
msgctxt "@title"
msgid "Maximum CPU Frequency"
msgstr "Maksimalna frekvenca CPE"

#: cpu/cpu.cpp:120
#, kde-format
msgctxt "@title, Short for 'Maximum CPU Frequency'"
msgid "Max Frequency"
msgstr "Maks. frekvenca"

#: cpu/cpu.cpp:121
#, kde-format
msgctxt "@info"
msgid "Current maximum frequency between all CPUs"
msgstr "Trenutna maks. frekvenca vseh CPE"

#: cpu/cpu.cpp:126
#, kde-format
msgctxt "@title"
msgid "Minimum CPU Frequency"
msgstr "Minimalna frekvenca CPE"

#: cpu/cpu.cpp:127
#, kde-format
msgctxt "@title, Short for 'Minimum CPU Frequency'"
msgid "Min Frequency"
msgstr "Min. frekvenca"

#: cpu/cpu.cpp:128
#, kde-format
msgctxt "@info"
msgid "Current minimum frequency between all CPUs"
msgstr "Trenutna minimalna frekvenca vseh CPE"

#: cpu/cpu.cpp:133
#, kde-format
msgctxt "@title"
msgid "Average CPU Frequency"
msgstr "Povprečna frekvenca CPE"

#: cpu/cpu.cpp:134
#, kde-format
msgctxt "@title, Short for 'Average CPU Frequency'"
msgid "Average Frequency"
msgstr "Povprečna frekvenca"

#: cpu/cpu.cpp:135
#, kde-format
msgctxt "@info"
msgid "Current average frequency between all CPUs"
msgstr "Trenutna povprečna frekvenca vseh CPE"

#: cpu/cpu.cpp:140
#, kde-format
msgctxt "@title"
msgid "Maximum CPU Temperature"
msgstr "Maksimalna temperatura CPE"

#: cpu/cpu.cpp:141
#, kde-format
msgctxt "@title, Short for 'Maximum CPU Temperature'"
msgid "Max Temperature"
msgstr "Maks. temperatura"

#: cpu/cpu.cpp:147
#, kde-format
msgctxt "@title"
msgid "Minimum CPU Temperature"
msgstr "Minimalna temperatura CPE"

#: cpu/cpu.cpp:148
#, kde-format
msgctxt "@title, Short for 'Minimum CPU Temperature'"
msgid "Min Temperature"
msgstr "Min. temperatura"

#: cpu/cpu.cpp:154
#, kde-format
msgctxt "@title"
msgid "Average CPU Temperature"
msgstr "Povprečna temperatura CPE"

#: cpu/cpu.cpp:155
#, kde-format
msgctxt "@title, Short for 'Average CPU Temperature'"
msgid "Average Temperature"
msgstr "Povprečna temperatura"

#: cpu/cpu.cpp:171
#, kde-format
msgctxt "@title"
msgid "Number of CPUs"
msgstr "Število CPE"

#: cpu/cpu.cpp:172
#, kde-format
msgctxt "@title, Short fort 'Number of CPUs'"
msgid "CPUs"
msgstr "CPE"

#: cpu/cpu.cpp:173
#, kde-format
msgctxt "@info"
msgid "Number of physical CPUs installed in the system"
msgstr "Število fizičnih CPE nameščenih na sistemu"

#: cpu/cpu.cpp:175
#, kde-format
msgctxt "@title"
msgid "Number of Cores"
msgstr "Število jeder"

#: cpu/cpu.cpp:176
#, kde-format
msgctxt "@title, Short fort 'Number of Cores'"
msgid "Cores"
msgstr "Jedra"

#: cpu/cpu.cpp:177
#, kde-format
msgctxt "@info"
msgid "Number of CPU cores across all physical CPUS"
msgstr "Število vseh jeder CPE izmed vseh fizičnih CPE"

#: cpu/cpuplugin.cpp:19
#, kde-format
msgid "CPUs"
msgstr "CPE"

#: cpu/freebsdcpuplugin.cpp:151
#, kde-format
msgctxt "@title"
msgid "CPU %1"
msgstr "CPE %1"

#: cpu/linuxcpuplugin.cpp:39
#, kde-format
msgctxt "@title"
msgid "Core %1"
msgstr "Jedro %1"

#: cpu/linuxcpuplugin.cpp:54
#, kde-format
msgctxt "@title"
msgid "CPU %1 Core %2"
msgstr "CPE %1 jedro %2"

#: cpu/loadaverages.cpp:13
#, kde-format
msgctxt "@title"
msgid "Load Averages"
msgstr "Naloži povprečja"

#: cpu/loadaverages.cpp:14
#, kde-format
msgctxt "@title"
msgid "Load average (1 minute)"
msgstr "Naloži povprečja (1 minuta)"

#: cpu/loadaverages.cpp:15
#, kde-format
msgctxt "@title"
msgid "Load average (5 minutes)"
msgstr "Naloži povprečja (5 minut)"

#: cpu/loadaverages.cpp:16
#, kde-format
msgctxt "@title"
msgid "Load average (15 minute)"
msgstr "Naloži povprečja (15 minut)"

#: cpu/loadaverages.cpp:18
#, kde-format
msgctxt "@title,  Short for 'Load average (1 minute)"
msgid "Load average (1m)"
msgstr "Naloži povprečja (1m)"

#: cpu/loadaverages.cpp:19
#, kde-format
msgctxt "@title,  Short for 'Load average (5 minutes)"
msgid "Load average (5m)"
msgstr "Naloži povprečja (5m)"

#: cpu/loadaverages.cpp:20
#, kde-format
msgctxt "@title,  Short for 'Load average (15 minutes)"
msgid "Load average (15m)"
msgstr "Naloži povprečja (15m)"

#: cpu/loadaverages.cpp:22
#, kde-format
msgctxt "@info"
msgid "Number of jobs in the run queue averaged over 1 minute"
msgstr "Število poslov v čakalni vrsti za izvajanje v eni minuti"

#: cpu/loadaverages.cpp:23
#, kde-format
msgctxt "@info"
msgid "Number of jobs in the run queue averaged over 5 minutes"
msgstr "Število poslov v čakalni vrsti za izvajanje v 5 minutah"

#: cpu/loadaverages.cpp:24
#, kde-format
msgctxt "@info"
msgid "Number of jobs in the run queue averaged over 15 minutes"
msgstr "Število poslov v čakalni vrsti za izvajanje v 15 minutah"

#: disks/disks.cpp:71 disks/disks.cpp:236
#, kde-format
msgctxt "@title"
msgid "Total Space"
msgstr "Ves prostor"

#: disks/disks.cpp:73 disks/disks.cpp:237
#, kde-format
msgctxt "@title Short for 'Total Space'"
msgid "Total"
msgstr "Skupaj"

#: disks/disks.cpp:77 disks/disks.cpp:249
#, kde-format
msgctxt "@title"
msgid "Used Space"
msgstr "Porabljen prostor"

#: disks/disks.cpp:79 disks/disks.cpp:250
#, kde-format
msgctxt "@title Short for 'Used Space'"
msgid "Used"
msgstr "Porabljeno"

#: disks/disks.cpp:84 disks/disks.cpp:242
#, kde-format
msgctxt "@title"
msgid "Free Space"
msgstr "Prost prostor"

#: disks/disks.cpp:86 disks/disks.cpp:243
#, kde-format
msgctxt "@title Short for 'Free Space'"
msgid "Free"
msgstr "Prosto"

#: disks/disks.cpp:91 disks/disks.cpp:256
#, kde-format
msgctxt "@title"
msgid "Read Rate"
msgstr "Hitrost branja"

#: disks/disks.cpp:93 disks/disks.cpp:257
#, kde-format
msgctxt "@title Short for 'Read Rate'"
msgid "Read"
msgstr "Branje"

#: disks/disks.cpp:97 disks/disks.cpp:262
#, kde-format
msgctxt "@title"
msgid "Write Rate"
msgstr "Hitrost pisanja"

#: disks/disks.cpp:99 disks/disks.cpp:263
#, kde-format
msgctxt "@title Short for 'Write Rate'"
msgid "Write"
msgstr "Pisanje"

#: disks/disks.cpp:103 disks/disks.cpp:272
#, kde-format
msgctxt "@title"
msgid "Percentage Used"
msgstr "Odstotek uporabljenega"

#: disks/disks.cpp:107 disks/disks.cpp:268
#, kde-format
msgctxt "@title"
msgid "Percentage Free"
msgstr "Odstotek prostega"

#: disks/disks.cpp:142
#, kde-format
msgid "Disks"
msgstr "Diski"

#: disks/disks.cpp:234
#, kde-format
msgctxt "@title"
msgid "All Disks"
msgstr "Vsi diski"

#: disks/disks.cpp:269
#, kde-format
msgctxt "@title, Short for `Percentage Free"
msgid "Free"
msgstr "Prosto"

#: disks/disks.cpp:273
#, kde-format
msgctxt "@title, Short for `Percentage Used"
msgid "Used"
msgstr "Porabljeno"

#: gpu/AllGpus.cpp:14
#, kde-format
msgctxt "@title"
msgid "All GPUs"
msgstr "Vse GPE"

#: gpu/AllGpus.cpp:16
#, kde-format
msgctxt "@title"
msgid "All GPUs Usage"
msgstr "Poraba vseh GPE"

#: gpu/AllGpus.cpp:17
#, kde-format
msgctxt "@title Short for 'All GPUs Usage'"
msgid "Usage"
msgstr "Poraba"

#: gpu/AllGpus.cpp:26
#, kde-format
msgctxt "@title"
msgid "All GPUs Total Memory"
msgstr "Celoten pomnilnik vseh GPE"

#: gpu/AllGpus.cpp:27
#, kde-format
msgctxt "@title Short for 'All GPUs Total Memory'"
msgid "Total"
msgstr "Skupaj"

#: gpu/AllGpus.cpp:31
#, kde-format
msgctxt "@title"
msgid "All GPUs Used Memory"
msgstr "Porabljen pomnilnik vseh GPE"

#: gpu/AllGpus.cpp:32
#, kde-format
msgctxt "@title Short for 'All GPUs Used Memory'"
msgid "Used"
msgstr "Uporabljeno"

#: gpu/GpuDevice.cpp:24
#, kde-format
msgctxt "@title"
msgid "Usage"
msgstr "Poraba"

#: gpu/GpuDevice.cpp:30
#, kde-format
msgctxt "@title"
msgid "Total Video Memory"
msgstr "Celoten video pomnilnik"

#: gpu/GpuDevice.cpp:32
#, kde-format
msgctxt "@title Short for Total Video Memory"
msgid "Total"
msgstr "Skupaj"

#: gpu/GpuDevice.cpp:35
#, kde-format
msgctxt "@title"
msgid "Video Memory Used"
msgstr "Poraba video pomnilnika"

#: gpu/GpuDevice.cpp:37
#, kde-format
msgctxt "@title Short for Video Memory Used"
msgid "Used"
msgstr "Uporabljeno"

#: gpu/GpuDevice.cpp:41
#, kde-format
msgctxt "@title"
msgid "Frequency"
msgstr "Frekvenca"

#: gpu/GpuDevice.cpp:45
#, kde-format
msgctxt "@title"
msgid "Memory Frequency"
msgstr "Frekvenca pomnilnika"

#: gpu/GpuDevice.cpp:49
#, kde-format
msgctxt "@title"
msgid "Temperature"
msgstr "Temperatura"

#: gpu/GpuDevice.cpp:53 power/power.cpp:106
#, kde-format
msgctxt "@title"
msgid "Power"
msgstr "Moč"

#: gpu/GpuPlugin.cpp:31
#, kde-format
msgctxt "@title"
msgid "GPU"
msgstr "GPE"

#: gpu/LinuxBackend.cpp:54
#, kde-format
msgctxt "@title %1 is GPU number"
msgid "GPU %1"
msgstr "GPE %1"

#: lmsensors/lmsensors.cpp:22
#, kde-format
msgid "Hardware Sensors"
msgstr "Strojni senzorji"

#: memory/backend.cpp:17
#, kde-format
msgctxt "@title"
msgid "Physical Memory"
msgstr "Fizični pomnilnik"

#: memory/backend.cpp:18
#, kde-format
msgctxt "@title"
msgid "Swap Memory"
msgstr "Izmenjevalni pomnilnik"

#: memory/backend.cpp:39
#, kde-format
msgctxt "@title"
msgid "Total Physical Memory"
msgstr "Celotni fizični pomnilnik"

#: memory/backend.cpp:40
#, kde-format
msgctxt "@title, Short for 'Total Physical Memory'"
msgid "Total"
msgstr "Skupaj"

#: memory/backend.cpp:44
#, kde-format
msgctxt "@title"
msgid "Used Physical Memory"
msgstr "Uporabljen fizični pomnilnik"

#: memory/backend.cpp:45
#, kde-format
msgctxt "@title, Short for 'Used Physical Memory'"
msgid "Used"
msgstr "Uporabljeno"

#: memory/backend.cpp:49
#, kde-format
msgctxt "@title"
msgid "Used Physical Memory Percentage"
msgstr "Odstotek uporabljenega fizičnega pomnilnika"

#: memory/backend.cpp:53
#, kde-format
msgctxt "@title"
msgid "Free Physical Memory"
msgstr "Prosti fizični pomnilnik"

#: memory/backend.cpp:54
#, kde-format
msgctxt "@title, Short for 'Free Physical Memory'"
msgid "Free"
msgstr "Prosto"

#: memory/backend.cpp:58
#, kde-format
msgctxt "@title"
msgid "Free Physical Memory Percentage"
msgstr "Odstotek prostega fizičnega pomnilnika"

#: memory/backend.cpp:62
#, kde-format
msgctxt "@title"
msgid "Application Memory"
msgstr "Pomnilnik za aplikacijo"

#: memory/backend.cpp:63
#, kde-format
msgctxt "@title, Short for 'Application Memory'"
msgid "Application"
msgstr "Aplikacija"

#: memory/backend.cpp:67
#, kde-format
msgctxt "@title"
msgid "Application Memory Percentage"
msgstr "Odstotek pomnilnika za aplikacijo"

#: memory/backend.cpp:71
#, kde-format
msgctxt "@title"
msgid "Cache Memory"
msgstr "Predpomnilnik"

#: memory/backend.cpp:72
#, kde-format
msgctxt "@title, Short for 'Cache Memory'"
msgid "Cache"
msgstr "Predpomnilnik"

#: memory/backend.cpp:76
#, kde-format
msgctxt "@title"
msgid "Cache Memory Percentage"
msgstr "Odstotek pomnilnika za predpomnilnik"

#: memory/backend.cpp:80
#, kde-format
msgctxt "@title"
msgid "Buffer Memory"
msgstr "Vmesni pomnilnik"

#: memory/backend.cpp:81
#, kde-format
msgctxt "@title, Short for 'Buffer Memory'"
msgid "Buffer"
msgstr "Vmesni pomnilnik"

#: memory/backend.cpp:82
#, kde-format
msgid "Amount of memory used for caching disk blocks"
msgstr "Količina pomnilnika uporabljenega za predpomnenje diskovnih blokov"

#: memory/backend.cpp:86
#, kde-format
msgctxt "@title"
msgid "Buffer Memory Percentage"
msgstr "Odstotek vmesnega pomnilnika"

#: memory/backend.cpp:90
#, kde-format
msgctxt "@title"
msgid "Total Swap Memory"
msgstr "Celotni izmenjevalni pomnilnik"

#: memory/backend.cpp:91
#, kde-format
msgctxt "@title, Short for 'Total Swap Memory'"
msgid "Total"
msgstr "Skupaj"

#: memory/backend.cpp:95
#, kde-format
msgctxt "@title"
msgid "Used Swap Memory"
msgstr "Uporabljen izmenjevalni pomnilnik"

#: memory/backend.cpp:96
#, kde-format
msgctxt "@title, Short for 'Used Swap Memory'"
msgid "Used"
msgstr "Uporabljeno"

#: memory/backend.cpp:100
#, kde-format
msgctxt "@title"
msgid "Used Swap Memory Percentage"
msgstr "Odstotek uporabljenega izmenjevalnega pomnilnika"

#: memory/backend.cpp:104
#, kde-format
msgctxt "@title"
msgid "Free Swap Memory"
msgstr "Prosti izmenjevalni pomnilnik"

#: memory/backend.cpp:105
#, kde-format
msgctxt "@title, Short for 'Free Swap Memory'"
msgid "Free"
msgstr "Prosto"

#: memory/backend.cpp:109
#, kde-format
msgctxt "@title"
msgid "Free Swap Memory Percentage"
msgstr "Odstotek prostega izmenjevalnega pomnilnika"

#: memory/memory.cpp:24
#, kde-format
msgctxt "@title"
msgid "Memory"
msgstr "Pomnilnik"

#: network/AllDevicesObject.cpp:16
#, kde-format
msgctxt "@title"
msgid "All Network Devices"
msgstr "Vse mrežne naprave"

#: network/AllDevicesObject.cpp:18 network/AllDevicesObject.cpp:28
#: network/NetworkDevice.cpp:66 network/NetworkDevice.cpp:76
#, kde-format
msgctxt "@title"
msgid "Download Rate"
msgstr "Hitrost prenosa sem"

#: network/AllDevicesObject.cpp:19 network/AllDevicesObject.cpp:29
#: network/NetworkDevice.cpp:67 network/NetworkDevice.cpp:77
#, kde-format
msgctxt "@title Short for Download Rate"
msgid "Download"
msgstr "Prenesi sem"

#: network/AllDevicesObject.cpp:23 network/AllDevicesObject.cpp:33
#: network/NetworkDevice.cpp:71 network/NetworkDevice.cpp:81
#, kde-format
msgctxt "@title"
msgid "Upload Rate"
msgstr "Hitrost prenosa tja"

#: network/AllDevicesObject.cpp:24 network/AllDevicesObject.cpp:34
#: network/NetworkDevice.cpp:72 network/NetworkDevice.cpp:82
#, kde-format
msgctxt "@title Short for Upload Rate"
msgid "Upload"
msgstr "Prenesi tja"

#: network/AllDevicesObject.cpp:38 network/NetworkDevice.cpp:86
#, kde-format
msgctxt "@title"
msgid "Total Downloaded"
msgstr "Vsega preneseno sem"

#: network/AllDevicesObject.cpp:39 network/NetworkDevice.cpp:87
#, kde-format
msgctxt "@title Short for Total Downloaded"
msgid "Downloaded"
msgstr "Preneseno sem"

#: network/AllDevicesObject.cpp:43 network/NetworkDevice.cpp:91
#, kde-format
msgctxt "@title"
msgid "Total Uploaded"
msgstr "Skupaj preneseno tja"

#: network/AllDevicesObject.cpp:44 network/NetworkDevice.cpp:92
#, kde-format
msgctxt "@title Short for Total Uploaded"
msgid "Uploaded"
msgstr "Preneseno tja"

#: network/NetworkDevice.cpp:15
#, kde-format
msgctxt "@title"
msgid "Network Name"
msgstr "Ime omrežja"

#: network/NetworkDevice.cpp:16
#, kde-format
msgctxt "@title Short of Network Name"
msgid "Name"
msgstr "Ime"

#: network/NetworkDevice.cpp:19
#, kde-format
msgctxt "@title"
msgid "Signal Strength"
msgstr "Moč signala"

#: network/NetworkDevice.cpp:20
#, kde-format
msgctxt "@title Short of Signal Strength"
msgid "Signal"
msgstr "Signal"

#: network/NetworkDevice.cpp:26
#, kde-format
msgctxt "@title"
msgid "IPv4 Address"
msgstr "Naslov IPv4"

#: network/NetworkDevice.cpp:27
#, kde-format
msgctxt "@title Short of IPv4 Address"
msgid "IPv4"
msgstr "IPv4"

#: network/NetworkDevice.cpp:30
#, kde-format
msgctxt "@title"
msgid "IPv4 Gateway"
msgstr "Prehod IPv4"

#: network/NetworkDevice.cpp:31
#, kde-format
msgctxt "@title Short of IPv4 Gateway"
msgid "IPv4 Gateway"
msgstr "Prehod IPv4"

#: network/NetworkDevice.cpp:34
#, kde-format
msgctxt "@title"
msgid "IPv4 Subnet Mask"
msgstr "Maska podmreže IPv4"

#: network/NetworkDevice.cpp:35
#, kde-format
msgctxt "@title Short of IPv4 Subnet Mask"
msgid "IPv4 Subnet Mask"
msgstr "Maska podmreže IPv4"

#: network/NetworkDevice.cpp:38
#, kde-format
msgctxt "@title"
msgid "IPv4 with Prefix Length"
msgstr "IPv4 z dolžino predpone"

#: network/NetworkDevice.cpp:39
#, kde-format
msgctxt "@title Short of IPv4 Prefix Length"
msgid "IPv4"
msgstr "IPv4"

#: network/NetworkDevice.cpp:42
#, kde-format
msgctxt "@title"
msgid "IPv4 DNS"
msgstr "IPv4 DNS"

#: network/NetworkDevice.cpp:43
#, kde-format
msgctxt "@title Short of IPv4 DNS"
msgid "IPv4 DNS"
msgstr "IPv4 DNS"

#: network/NetworkDevice.cpp:46
#, kde-format
msgctxt "@title"
msgid "IPv6 Address"
msgstr "Naslov IPv6"

#: network/NetworkDevice.cpp:47
#, kde-format
msgctxt "@title Short of IPv6 Address"
msgid "IPv6"
msgstr "IPv6"

#: network/NetworkDevice.cpp:50
#, kde-format
msgctxt "@title"
msgid "IPv6 Gateway"
msgstr "Prehod IPv6"

#: network/NetworkDevice.cpp:51
#, kde-format
msgctxt "@title Short of IPv6 Gateway"
msgid "IPv6 Gateway"
msgstr "Prehod IPv6"

#: network/NetworkDevice.cpp:54
#, kde-format
msgctxt "@title"
msgid "IPv6 Subnet Mask"
msgstr "Maska podmreže IPv6"

#: network/NetworkDevice.cpp:55
#, kde-format
msgctxt "@title Short of IPv6 Subnet Mask"
msgid "IPv6 Subnet Mask"
msgstr "Maska podmreže IPv6"

#: network/NetworkDevice.cpp:58
#, kde-format
msgctxt "@title"
msgid "IPv6 with Prefix Length"
msgstr "IPv6 z dolžino predpone"

#: network/NetworkDevice.cpp:59
#, kde-format
msgctxt "@title Short of IPv6 Prefix Length"
msgid "IPv6"
msgstr "IPv6"

#: network/NetworkDevice.cpp:62
#, kde-format
msgctxt "@title"
msgid "IPv6 DNS"
msgstr "IPv6 DNS"

#: network/NetworkDevice.cpp:63
#, kde-format
msgctxt "@title Short of IPv6 DNS"
msgid "IPv6 DNS"
msgstr "IPv6 DNS"

#: network/NetworkPlugin.cpp:43
#, kde-format
msgctxt "@title"
msgid "Network Devices"
msgstr "Omrežne naprave"

#: osinfo/osinfo.cpp:106
#, kde-format
msgctxt "@title"
msgid "Operating System"
msgstr "Operacijski sistem"

#: osinfo/osinfo.cpp:108
#, kde-format
msgctxt "@title"
msgid "Kernel"
msgstr "Jedro"

#: osinfo/osinfo.cpp:109
#, kde-format
msgctxt "@title"
msgid "Kernel Name"
msgstr "Ime jedra"

#: osinfo/osinfo.cpp:110
#, kde-format
msgctxt "@title"
msgid "Kernel Version"
msgstr "Verzija jedra"

#: osinfo/osinfo.cpp:111
#, kde-format
msgctxt "@title"
msgid "Kernel Name and Version"
msgstr "Ime jedra in verzija"

#: osinfo/osinfo.cpp:112
#, kde-format
msgctxt "@title Kernel Name and Version"
msgid "Kernel"
msgstr "Jedro"

#: osinfo/osinfo.cpp:114
#, kde-format
msgctxt "@title"
msgid "System"
msgstr "Sistem"

#: osinfo/osinfo.cpp:115
#, kde-format
msgctxt "@title"
msgid "Hostname"
msgstr "Ime gostitelja"

#: osinfo/osinfo.cpp:116
#, kde-format
msgctxt "@title"
msgid "Operating System Name"
msgstr "Ime operacijskega sistema"

#: osinfo/osinfo.cpp:117
#, kde-format
msgctxt "@title"
msgid "Operating System Version"
msgstr "Verzija operacijskega sistema"

#: osinfo/osinfo.cpp:118
#, kde-format
msgctxt "@title"
msgid "Operating System Name and Version"
msgstr "Ime operacijskega sistema in verzija"

#: osinfo/osinfo.cpp:119
#, kde-format
msgctxt "@title Operating System Name and Version"
msgid "OS"
msgstr "OS"

#: osinfo/osinfo.cpp:120
#, kde-format
msgctxt "@title"
msgid "Operating System Logo"
msgstr "Logotip operacijskega sistema"

#: osinfo/osinfo.cpp:121
#, kde-format
msgctxt "@title"
msgid "Operating System URL"
msgstr "Spletni naslov operacijskega sistema"

#: osinfo/osinfo.cpp:122
#, kde-format
msgctxt "@title"
msgid "Uptime"
msgstr "Čas neprekinjenega delovanja"

#: osinfo/osinfo.cpp:125
#, kde-format
msgctxt "@title"
msgid "KDE Plasma"
msgstr "KDE Plasma"

#: osinfo/osinfo.cpp:126
#, kde-format
msgctxt "@title"
msgid "Qt Version"
msgstr "Qt verzija"

#: osinfo/osinfo.cpp:127
#, kde-format
msgctxt "@title"
msgid "KDE Frameworks Version"
msgstr "KDE Frameworks verzija"

#: osinfo/osinfo.cpp:128
#, kde-format
msgctxt "@title"
msgid "KDE Plasma Version"
msgstr "KDE Plasma verzija"

#: osinfo/osinfo.cpp:129
#, kde-format
msgctxt "@title"
msgid "Window System"
msgstr "Sistem oken"

#: osinfo/osinfo.cpp:163
#, kde-format
msgctxt "@info"
msgid "Unknown"
msgstr "Neznano"

#: power/power.cpp:44 power/power.cpp:45
#, kde-format
msgctxt "@title"
msgid "Design Capacity"
msgstr "Zasnova zmogljivosti"

#: power/power.cpp:47
#, kde-format
msgid "Amount of energy that the Battery was designed to hold"
msgstr "Količina energije, za katero je bila zasnovana baterija"

#: power/power.cpp:53 power/power.cpp:54 power/power.cpp:72
#, kde-format
msgctxt "@title"
msgid "Current Capacity"
msgstr "Trenutna zmogljivost"

#: power/power.cpp:56
#, kde-format
msgid "Amount of energy that the battery can currently hold"
msgstr "Količina energije, ki jo trenutno lahko drži baterija"

#: power/power.cpp:62 power/power.cpp:63
#, kde-format
msgctxt "@title"
msgid "Health"
msgstr "Zdravje"

#: power/power.cpp:65
#, kde-format
msgid "Percentage of the design capacity that the battery can hold"
msgstr "Odstotek zasnovane zmogljivosti, ki jo lahko drži baterija"

#: power/power.cpp:71
#, kde-format
msgctxt "@title"
msgid "Charge"
msgstr "Polni"

#: power/power.cpp:74
#, kde-format
msgid "Amount of energy that the battery is currently holding"
msgstr "Količina energije, ki jo trenutno ima baterija"

#: power/power.cpp:80 power/power.cpp:81
#, kde-format
msgctxt "@title"
msgid "Charge Percentage"
msgstr "Delež napolnjenosti"

#: power/power.cpp:83
#, kde-format
msgid ""
"Percentage of the current capacity that the battery is currently holding"
msgstr "Odstotek trenutne kapacitete baterije"

#: power/power.cpp:90
#, kde-format
msgctxt "@title"
msgid "Charging Rate"
msgstr "Hitrost polnjenja"

#: power/power.cpp:91
#, kde-format
msgctxt "@title"
msgid "Charging  Rate"
msgstr "Hitrost polnjenja"

#: power/power.cpp:93
#, kde-format
msgid ""
"Power that the battery is being charged with (positive) or discharged "
"(negative)"
msgstr "Moč, s katero se baterija polni (pozitivno) ali prazni (negativno)"

#~ msgctxt "@title %1 is a number"
#~ msgid "Voltage %1"
#~ msgstr "Napetost %1"

#~ msgctxt "@title %1 is a number"
#~ msgid "Fan %1"
#~ msgstr "Ventilator %1"

#~ msgctxt "@title %1 is a number"
#~ msgid "Temperature %1"
#~ msgstr "Temperatura %1"

#~ msgctxt "@info"
#~ msgid "Percentage of memory taken by applications."
#~ msgstr "Odstotek pomnilnika, ki ga zasedajo aplikacije."

#~ msgctxt "@info"
#~ msgid "Percentage of memory taken by the buffer."
#~ msgstr "Odstotek pomnilnika, ki ga zavzema vmesni pomnilnik."

#~ msgctxt "@info"
#~ msgid "Percentage of memory taken by the cache."
#~ msgstr "Odstotek pomnilnika, ki ga zaseda predpomnilnik."

#~ msgctxt "@title Free Memory Percentage"
#~ msgid "Free"
#~ msgstr "Prosto"

#~ msgctxt "@info"
#~ msgid "Percentage of free memory."
#~ msgstr "Odstotek prostega pomnilnika."

#~ msgctxt "@title Used Memory Percentage"
#~ msgid "Used"
#~ msgstr "Porabljeno"

#~ msgctxt "@info"
#~ msgid "Percentage of used memory."
#~ msgstr "Odstotek uporabljenega pomnilnika."

#~ msgctxt "@title"
#~ msgid "Available Memory Percentage"
#~ msgstr "Odstotek pomnilnika, ki je na voljo"

#~ msgctxt "@title Available Memory Percentage"
#~ msgid "Available"
#~ msgstr "Na voljo"

#~ msgctxt "@info"
#~ msgid "Percentage of available memory."
#~ msgstr "Odstotek pomnilnika, ki je na voljo."

#~ msgctxt "@title"
#~ msgid "Allocated Memory Percentage"
#~ msgstr "Dodeljeni odstotek pomnilnika"

#~ msgctxt "@title Allocated Memory Percentage"
#~ msgid "Allocated"
#~ msgstr "Dodeljeno"

#~ msgctxt "@info"
#~ msgid "Percentage of allocated memory."
#~ msgstr "Odstotek dodeljenega pomnilnika."

#~ msgctxt "@title Total CPU Usage"
#~ msgid "Usage"
#~ msgstr "Poraba"

#~ msgctxt "@title Total Memory Usage"
#~ msgid "Total Used"
#~ msgstr "Skupaj uporabljeno"

#~ msgctxt "@title Cached Memory Usage"
#~ msgid "Cached"
#~ msgstr "Predpomnjeno"

#~ msgctxt "@title Free Memory Amount"
#~ msgid "Free"
#~ msgstr "Prosto"

#~ msgctxt "@title Available Memory Amount"
#~ msgid "Available"
#~ msgstr "Na voljo"

#~ msgctxt "@title Application Memory Usage"
#~ msgid "Application"
#~ msgstr "Aplikacija"

#~ msgctxt "@title Buffer Memory Usage"
#~ msgid "Buffer"
#~ msgstr "Vmesni pomnilnik"

#~ msgctxt "@title Number of Processors"
#~ msgid "Processors"
#~ msgstr "Procesorji"

#~ msgctxt "@title Number of Cores"
#~ msgid "Cores"
#~ msgstr "Jedra"

#~ msgctxt "@title"
#~ msgid "GPU %1 Power Usage"
#~ msgstr "Poraba moči za GPE %1"

#~ msgctxt "@title GPU Power Usage"
#~ msgid "Power"
#~ msgstr "Moč"

#~ msgctxt "@title GPU Temperature"
#~ msgid "Temperature"
#~ msgstr "Temperatura"

#~ msgctxt "@title"
#~ msgid "GPU %1 Shared Memory Usage"
#~ msgstr "Uporaba skupnega pomnilnika za GPE %1"

#~ msgctxt "@title"
#~ msgid "GPU %1 Encoder Usage"
#~ msgstr "Raba kodirnika GPE %1"

#~ msgctxt "@title GPU Encoder Usage"
#~ msgid "Encoder"
#~ msgstr "Kodirnik"

#~ msgctxt "@title"
#~ msgid "GPU %1 Decoder Usage"
#~ msgstr "Raba dekodirnika GPE %1"

#~ msgctxt "@title GPU Decoder Usage"
#~ msgid "Decoder"
#~ msgstr "Dekodirnik"

#~ msgctxt "@title"
#~ msgid "GPU %1 Memory Clock"
#~ msgstr "Pomnilniška ura GPE %1"

#~ msgctxt "@title GPU Memory Clock"
#~ msgid "Memory Clock"
#~ msgstr "Pomnilniška ura"

#~ msgctxt "@title"
#~ msgid "GPU %1 Processor Clock"
#~ msgstr "Procesorska ura GPE %1"

#~ msgctxt "@title GPU Processor Clock"
#~ msgid "Processor Clock"
#~ msgstr "Procesorska ura"

#~ msgctxt "@title NVidia GPU information"
#~ msgid "NVidia"
#~ msgstr "NVidia"

#~ msgctxt "@title"
#~ msgid "Disk Read Accesses"
#~ msgstr "Bralnih dostopov do diska"

#~ msgctxt "@info"
#~ msgid "Read accesses across all disk devices"
#~ msgstr "Bralnih dostopov na vseh diskovnih napravah"

#~ msgctxt "@title"
#~ msgid "Disk Write Accesses"
#~ msgstr "Pisalnih dostopov do diska"

#~ msgctxt "@info"
#~ msgid "Write accesses across all disk devices"
#~ msgstr "Pisalnih dostopov na vseh diskovnih napravah"

#~ msgctxt "@title All Network Interfaces"
#~ msgid "All"
#~ msgstr "Vse"

#~ msgctxt "@title"
#~ msgid "Received Data Rate"
#~ msgstr "Hitrost prejetih podatkov"

#~ msgctxt "@info"
#~ msgid "The rate at which data is received on all interfaces."
#~ msgstr "Hitrost prejema podatkov na vseh vmesnikih."

#~ msgctxt "@title"
#~ msgid "Total Received Data"
#~ msgstr "Vseh prejetih podatkov"

#~ msgctxt "@info"
#~ msgid "The total amount of data received on all interfaces."
#~ msgstr "Celotna količina prejetih podatkov na vseh vmesnikih."

#~ msgctxt "@title"
#~ msgid "Sent Data Rate"
#~ msgstr "Hitrost pošiljanja podatkov"

#~ msgctxt "@title Sent Data Rate"
#~ msgid "Up"
#~ msgstr "Tja"

#~ msgctxt "@info"
#~ msgid "The rate at which data is sent on all interfaces."
#~ msgstr "Hitrost pošiljanja podatkov na vseh vmesnikih."

#~ msgctxt "@title"
#~ msgid "Total Sent Data"
#~ msgstr "Vseh poslanih podatkov"

#~ msgctxt "@info"
#~ msgid "The total amount of data sent on all interfaces."
#~ msgstr "Celotna količina poslanih podatkov na vseh vmesnikih."
